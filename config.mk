projectName := dsihkal
version := 0.0.0

compilerFlags := -Wall -Wextra -std=c99 -pedantic -I inc -ggdb
debuggerFlags := -q
