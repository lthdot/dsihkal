include config.mk

headerFiles := $(wildcard inc/*.h)
sourceFiles := $(wildcard src/*.c)
testSourceFiles := $(wildcard tests/*.tests.c)

objectFiles := $(patsubst src/%.c, bin/%.o, $(sourceFiles))
binary := $(projectName).a

testObjectFiles := $(patsubst tests/%.c, bin/%.o, $(testSourceFiles))
testBinary := $(binary)-tests

default: build

debug: compilerFlags += -ggdb

bin/testmain.o: tests/testmain.c
	@echo -n "~ $< => $@ ... "
	@cc -D_TEST_MAIN $(compilerFlags) -c $< -o $@ && echo "done" || echo "failed"

bin/%.o: src/%.c inc/%.h
	@echo -n "~ $< => $@ ... "
	@cc $(compilerFlags) -c $< -o $@ && echo "done" || echo "failed"

bin/%.o: tests/%.c
	@echo -n "~ $< => $@ ... "
	@cc $(compilerFlags) -c $< -o $@ && echo "done" || echo "failed"

bin/$(binary): $(objectFiles) $(headerFiles)
	@echo -n "~ linking $@ ... "
	@ar rcs bin/$(binary) $(objectFiles) && echo "done" || echo "failed"

bin/$(testBinary): bin/testmain.o $(objectFiles) $(testObjectFiles) $(headerFiles) tests/tests.h
	@echo -n "~ linking $@ ... "
	@cc $(compilerFlags) $(objectFiles) $(testObjectFiles) bin/testmain.o -o bin/$(testBinary) && echo "done" || echo "failed"

build: bin/$(binary)

build-tests: bin/$(testBinary)

run-tests: bin/$(testBinary)
	@echo "> running $(projectName) $(version) tests"
	@bin/$(testBinary)

debug: bin/$(testBinary)
	@echo "> debugging $(projectName) $(version) tests"
	@gdb $(debuggerFlags) bin/$(testBinary)

dump:
	@echo "# projectName := $(projectName)"
	@echo "# version := $(version)"
	@echo "# compilerFlags := $(compilerFlags)"
	@echo "# sourceFiles := $(sourceFiles)"
	@echo "# objectFiles := $(objectFiles)"
	@echo "# binary := $(binary)"
	@echo "# testSourceFiles := $(testSourceFiles)"
	@echo "# testObjectFiles := $(testObjectFiles)"
	@echo "# testBinary := $(testBinary)"

clean:
	@echo "> cleaning"
	@rm -f bin/*
