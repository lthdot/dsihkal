#include <stdio.h>
#include "heap.h"

static void maxHeapify(Heap heap, int i);
static int selectIndex(Heap heap, int i, int j);
static void swap(Heap heap, int i, int j);

Heap Heap_BuildMax(int *elems, int size)
{
    Heap heap = { .elems = elems, .size = size };
    for (int i = (size - 1) / 2; i >= 0; --i) {
        maxHeapify(heap, i);
    }

    return heap;
}

static void maxHeapify(Heap heap, int i)
{
    bool done = false;
    while (!done) {
        int j = i;
        j = selectIndex(heap, j, Heap_LeftChild(i));
        j = selectIndex(heap, j, Heap_RightChild(i));

        if (i != j) {
            swap(heap, i, j);
            i = j;
        } else {
            done = true;
        }
    }
}

static int selectIndex(Heap heap, int i, int j)
{
    if (j < heap.size) {
        return heap.elems[j] > heap.elems[i] ? j : i;
    } else {
        return i;
    }
}

static void swap(Heap heap, int i, int j)
{
    int tmp = heap.elems[i];
    heap.elems[i] = heap.elems[j];
    heap.elems[j] = tmp;
}
