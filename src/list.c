#include <stdlib.h>

#include "list.h"

static ListElement *Element(const List *list, unsigned int index);

List List_New()
{
    List list = { .length = 0, .root = NULL };
    return list;
}

void List_Free(List list)
{
    ListElement *element = list.root, *next;
    for (unsigned int i = 0; i < list.length; ++i) {
        next = element->next;
        free(element);
        element = next;
    }
}

void List_Add(List *list, int value)
{
    ListElement *element;
    if (list->root) {
        element = Element(list, list->length - 1);
        element->next = malloc(sizeof(ListElement));
        element = element->next;
    } else {
        list->root = malloc(sizeof(ListElement));
        element = list->root;
    }
    element->value = value;
    element->next = NULL;
    list->length++;
}

bool List_Get(List list, unsigned int index, int *out)
{
    ListElement *element = Element(&list, index);
    if (element) {
        *out = element->value;
        return true;
    } else {
        return false;
    }
}

bool List_Remove(List *list, unsigned int index)
{
    ListElement *element = Element(list, index - 1);
    if (element && element->next) {
        ListElement *tmp = element->next->next;
        free(element->next);
        element->next = tmp;
        list->length--;
        return true;
    } else {
        return false;
    }
}

bool List_Head(List list, int *out)
{
    if (list.root) {
        *out = list.root->value;
        return true;
    } else {
        return false;
    }
}

bool List_Tail(List list, List *out)
{
    if (list.root) {
        out->length = list.length - 1;
        out->root = list.root->next;
        return true;
    } else {
        return false;
    }
}

static ListElement *Element(const List *list, unsigned int index)
{
    ListElement *element = list->root;
    for (unsigned int i = 0; i < index; ++i) {
        if (element) {
            element = element->next;
        } else {
            break;
        }
    }
    return element;
}
