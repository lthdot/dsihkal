#include <stdlib.h>
#include <string.h>

#include "array.h"

Array Array_New(unsigned int initalCapacity)
{
    Array array = { .capacity = initalCapacity, .length = 0 };
    array.data = calloc(initalCapacity, sizeof(int));
    return array;
}

void Array_Free(Array array)
{
    if (array.data != NULL) {
        free(array.data);
    }
}

bool Array_Grow(Array *array, int newCapacity)
{
    int *data = calloc(newCapacity, sizeof(int));
    if (data) {
        memcpy(data, array->data, array->length * sizeof(int));
        free(array->data);
        array->capacity = newCapacity;
        array->data = data;
        return true;
    } else {
        return false;
    }
}

bool Array_Add(Array *array, int element)
{
    if (array->capacity == array->length) {
        if (!Array_Grow(array, 2*array->capacity)) {
            return false;
        }
    }
    array->data[array->length] = element;
    array->length++;
    return true;
}
