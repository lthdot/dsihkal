#ifndef LIST_H
#define LIST_H

#include <stdbool.h>

typedef struct ListElement {
    int value;
    struct ListElement *next;
} ListElement;

typedef struct {
    unsigned int length;
    ListElement *root;
} List;

List List_New(void);
void List_Free(List list);

void List_Add(List *list, int value);
bool List_Get(List list, unsigned int index, int *out);
bool List_Remove(List *list, unsigned int index);

bool List_Head(List list, int *out);
bool List_Tail(List list, List *out);

#endif
