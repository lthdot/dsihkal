#ifndef HEAP_H
#define HEAP_H

#include <stdbool.h>

#define Heap_LeftChild(x) (2*(x) + 1)
#define Heap_RightChild(x) (2*(x) + 2)

typedef struct {
    int *elems;
    int size;
} Heap;

Heap Heap_BuildMax(int *elems, int size);

#endif
