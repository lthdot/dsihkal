#ifndef ARRAY_H
#define ARRAY_H

#include <stdbool.h>

typedef struct {
    unsigned int capacity;
    unsigned int length;
    int *data;
} Array;

Array Array_New(unsigned int initalCapacity);
void Array_Free(Array array);

bool Array_Grow(Array *array, int newCapacity);
bool Array_Add(Array *array, int element);

#endif
