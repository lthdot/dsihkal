#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "testmacros.h"
#include "heap.h"

TEST_GROUP(HeapTests)
{
    TEST(a_max_heap_can_be_built_from_an_unordered_array) {
        int elems[] = { 926, 342, 181, 843, 208, 421 };
        int elemsCopy[] = { 926, 342, 181, 843, 208, 421 };
        const int size = 6;

        Heap heap = Heap_BuildMax(elems, size);

        bool allElemsPresent = true;
        bool maxHeapProperty = true;
        for (int i = 0; i < size; ++i) {
            bool elemPresent = false;
            for (int j = 0; j < size; ++j) {
                elemPresent |= (elemsCopy[i] == heap.elems[j]);
            }
            allElemsPresent &= elemPresent;

            int k = Heap_LeftChild(i);
            if (k < size) {
                maxHeapProperty &= (heap.elems[i] >= heap.elems[k]);
            }

            k = Heap_RightChild(i);
            if (k < size) {
                maxHeapProperty &= (heap.elems[i] >= heap.elems[k]);
            }
        }

        ASSERT_TRUE(allElemsPresent);
        ASSERT_TRUE(maxHeapProperty);
    }
}
