#include "testmacros.h"
#include "array.h"

TEST_GROUP(ArrayTests)
{
    TEST(an_array_can_be_created_with_a_given_capacity) {
        Array array = Array_New(4);

        ASSERT_EQUAL(array.capacity, 4);

        Array_Free(array);
    }

    TEST(an_empty_array_has_length_0) {
        Array array = Array_New(4);

        ASSERT_EQUAL(array.length, 0);

        Array_Free(array);
    }

    TEST(an_array_can_be_grown_to_an_arbitrary_capacity) {
        Array array = Array_New(4);
        bool success = Array_Grow(&array, 8);

        ASSERT_TRUE(success);
        ASSERT_EQUAL(array.capacity, 8);

        Array_Free(array);
    }

    TEST(adding_elements_to_an_array_increases_its_length) {
        Array array = Array_New(4);
        bool success = true;
        success &= Array_Add(&array, 1);
        success &= Array_Add(&array, 2);
        success &= Array_Add(&array, 3);
        success &= Array_Add(&array, 4);

        ASSERT_TRUE(success);
        ASSERT_EQUAL(array.length, 4);

        Array_Free(array);
    }

    TEST(adding_more_elements_than_an_arrays_capacity_doubles_its_capactiy) {
        Array array = Array_New(4);
        bool success = true;
        success &= Array_Add(&array, 1);
        success &= Array_Add(&array, 2);
        success &= Array_Add(&array, 3);
        success &= Array_Add(&array, 4);
        success &= Array_Add(&array, 5);

        ASSERT_TRUE(success);
        ASSERT_EQUAL(array.capacity, 8);

        Array_Free(array);
    }
}
