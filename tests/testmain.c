#include <assert.h>
#include <stdio.h>

#include "testmacros.h"

TESTS
{
    RUN_GROUP(ListTests);
    RUN_GROUP(HeapTests);
    RUN_GROUP(ArrayTests);
}
