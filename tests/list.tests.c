#include <stdlib.h>

#include "tests.h"
#include "list.h"

TEST_GROUP(ListTests)
{
    TEST(an_empty_list_has_length_0) {
        List list = List_New();
        ASSERT_EQUAL(list.length, 0);
    }

    TEST(adding_elements_to_a_list_increases_its_length) {
        List list = List_New();
        List_Add(&list, 1);
        List_Add(&list, 2);
        List_Add(&list, 3);
        List_Add(&list, 4);

        ASSERT_EQUAL(list.length, 4);

        List_Free(list);
    }

    TEST(an_element_can_be_got_by_index) {
        List list = List_New();
        List_Add(&list, 1);
        List_Add(&list, 2);
        List_Add(&list, 3);
        List_Add(&list, 4);

        int element = 0;
        bool success = List_Get(list, 2, &element);
        ASSERT_TRUE(success);
        ASSERT_EQUAL(element, 3);

        List_Free(list);
    }

    TEST(getting_an_element_outside_the_bounds_of_a_list_fails) {
        List list = List_New();
        List_Add(&list, 1);
        List_Add(&list, 2);
        List_Add(&list, 3);
        List_Add(&list, 4);

        bool success = List_Get(list, 4, NULL);
        ASSERT_FALSE(success);

        List_Free(list);
    }

    TEST(an_element_can_be_removed_by_index) {
        List list = List_New();
        List_Add(&list, 1);
        List_Add(&list, 2);
        List_Add(&list, 3);
        List_Add(&list, 4);

        bool success = List_Remove(&list, 2);
        ASSERT_TRUE(success);

        int element;
        (void)List_Get(list, 0, &element);
        ASSERT_EQUAL(element, 1);
        (void)List_Get(list, 1, &element);
        ASSERT_EQUAL(element, 2);
        (void)List_Get(list, 2, &element);
        ASSERT_EQUAL(element, 4);

        ASSERT_EQUAL(list.length, 3);

        List_Free(list);
    }

    TEST(removing_an_element_outside_the_bounds_of_a_list_fails) {
        List list = List_New();
        List_Add(&list, 1);
        List_Add(&list, 2);
        List_Add(&list, 3);
        List_Add(&list, 4);

        bool success = List_Remove(&list, 4);
        ASSERT_FALSE(success);

        List_Free(list);
    }

    TEST(the_head_of_a_non_empty_list_can_be_got) {
        List list = List_New();
        List_Add(&list, 1);
        List_Add(&list, 2);
        List_Add(&list, 3);
        List_Add(&list, 4);

        int head;
        bool success = List_Head(list, &head);
        ASSERT_TRUE(success);
        ASSERT_EQUAL(head, 1);

        List_Free(list);
    }

    TEST(head_fails_on_empty_list) {
        List list = List_New();

        bool success = List_Head(list, NULL);
        ASSERT_FALSE(success);
    }

    TEST(the_tail_of_a_non_empty_list_can_be_got) {
        List list = List_New();
        List_Add(&list, 1);
        List_Add(&list, 2);
        List_Add(&list, 3);
        List_Add(&list, 4);

        List tail;
        bool success = List_Tail(list, &tail);
        ASSERT_TRUE(success);
        ASSERT_EQUAL(tail.length, 3);

        int element;
        (void)List_Get(tail, 0, &element);
        ASSERT_EQUAL(element, 2);
        (void)List_Get(tail, 1, &element);
        ASSERT_EQUAL(element, 3);
        (void)List_Get(tail, 2, &element);
        ASSERT_EQUAL(element, 4);

        List_Free(list);
    }

    TEST(tail_fails_on_empty_list) {
        List list = List_New();

        bool success = List_Tail(list, NULL);
        ASSERT_FALSE(success);
    }
}
