#ifndef TESTMACROS_H
#define TESTMACROS_H

#include <stdbool.h>

#define Q(X) #X
#define QUOTE(X) Q(X)

#define MAXIMUM_TEST_COUNT 128

#define TESTS void _Tests(void)

#define TEST_GROUP(T) void T(void)
#define RUN_GROUP(T) extern void T(void); T()

#define TEST(T) for ( \
                    bool _testFinished = false, _result = true; \
                    !_testFinished; \
                    _testFinished = true, _FinishTest(QUOTE(T), _result) \
                )

#define ASSERT_EQUAL(X, Y) _result = (X) == (Y); if (!_result) continue
#define ASSERT_NOT_EQUAL(X, Y) _result = (X) != (Y); if (!_result) continue
#define ASSERT_TRUE(P) _result = P; if (!_result) continue
#define ASSERT_FALSE(P) _result = !(P); if (!_result) continue

void _FinishTest(const char *testName, bool result);
void _Tests(void);

#ifdef _TEST_MAIN

static int _failCountG;
static int _testCountG;

void _FinishTest(const char *testName, bool result)
{
    _testCountG++;
    printf("* %s", testName);

    if (result) {
        printf("\n");
    } else {
        _failCountG++;
        printf(" FAILED\n");
    }
}

int main()
{
    _failCountG = _testCountG = 0;

    _Tests();

    if (_failCountG == 0) {
        printf("# all (%d) tests passed\n", _testCountG);
    } else {
        printf("# %d (of %d) tests failed\n", _failCountG, _testCountG);
    }

    return 0;
}

#endif

#endif
